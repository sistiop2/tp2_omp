/**
 * @file SO2_TP2_OMP.c
 * @author Ezequiel Zimmel (ezequielzimmel@gmail.com)
 * @brief Programa que realiza el filtrado de imagenes bajo extensión .nc (NetCDF).
 *        Se provee de flexibilidad mediante uso de flag en la ejecucion del programa.
 * 
 *        Ejecutar como ./BFilter -<Flag 1> ... -<Flag N>.
 *           Ejemplo:  ./BFilter -p -i
 *                  Ejecucion procedural, procesamiento de NaN y generacion de imagen .pgm
 * 
 *        Flags disponibles:
 *           -p Procesamiento de NaN en datos de entrada.
 *           -i Generacion de imagen .pgm post filtrado.
 *           -n Generacion de archivo .nc post filtrado.
 * 
 *        Por defecto el programa ejecuta con OMP_MAX_THREAD y sin flags activas.
 * 
 * @version 0.1
 * @date 2020-02-04
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "filter.h"

/**
 * @brief Verifica los argumento pasados en la ejecucion del programa y modifica
 *        su comportamiento en consecuencia.
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char *argv[])
{
    int i_flag = 0, p_flag = 0, n_flag = 0;
    double tiempo;
    float kernel[KERNEL_X][KERNEL_Y];
    const char delimiter[2] = "-";
    char *token;
    int NUM_THREADS = 0;
    FILE *tiempos;

    printf("==========================================\n");
    printf(" - Ejecutando de forma Procedural -\n");
    printf(" - Flag:                     -\n");
    if (argc > 1)
    {
        token = strtok(argv[1], delimiter);
        checkFlag(token, &i_flag, &p_flag, &n_flag);
    }
    if (argc > 2)
    {
        token = strtok(argv[2], delimiter);
        checkFlag(token, &i_flag, &p_flag, &n_flag);
    }
    if (argc > 3)
    {
        token = strtok(argv[3], delimiter);
        checkFlag(token, &i_flag, &p_flag, &n_flag);
    }

    // Reserva de Memoria
    printf(" - Alocando Memoria -\n");
    float *data_in = malloc(NX * NY * sizeof(float));
    float *data_out = malloc(NX * NY * sizeof(float));

    /* Almacena en un array los datos procedente del archivo .nc */
    loadPreFilter(data_in);

    /* Procesamos los NAN (Not A Number) de la imagen de entrada. */
    if (p_flag)
    {
        printf(" - Procesando NaN en data_in -\n");
        processNAN(data_in);
    }

    /* Seleccionamos el tipo de kernel para el filtrado. */
    selectKernel(kernel, KERNEL_EDGE_DETECTION_DIAGONAL);

    printf(" - Inicio de CONVOLUCIÓN -\n");
    convolutionFiltering(data_out, data_in, kernel, &tiempo);
    printf(" --- Tiempo empleado: %f --- \n", tiempo);

    char file_name[50];
    char num_th[4];
    sprintf(num_th, "%d", NUM_THREADS);

    strcpy(file_name, FILE_TIME_OUT);
    strcat(file_name, num_th);
    strcat(file_name, ".csv");

    tiempos = fopen(file_name, "a");
    fprintf(tiempos, "%f\n", tiempo);
    fclose(tiempos);

    if (i_flag)
    {
        printf(" - Generando imagen de salida .pgm -\n");
        saveAsImage(data_out);
    }

    if (n_flag)
    {
        printf(" - Generando archivo de salida .nc -\n");
        savePostFilter(data_out);
    }

    free(data_in);
    free(data_out);

    return 0;
}
