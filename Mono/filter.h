/**
 * @file filter.h
 * @author Ezequiel Zimmel (ezequielzimmel@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-02-04
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <netcdf.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

/* Handle errors by printing an error message and exiting kernelith a
 * non-zero status. */
#define ERRCODE 2
#define ERR(e)                                 \
    {                                          \
        printf("Error: %s\n", nc_strerror(e)); \
        exit(ERRCODE);                         \
    }

/* nombre del archivo a leer */
#define FILE_NAME_IN "../recursos/OR_ABI-L2-CMIPF-M6C02_G16_s20191011800206_e20191011809514_c20191011809591.nc"
#define FILE_NAME_OUT "../recursos/BFilter_out.nc"
#define IMG_NAME_OUT "../recursos/BFilter_out.pgm"
#define FILE_TIME_OUT "../recursos/Tiempo_P_Local_Core_"
//#define FILE_TIME_OUT "recursos/Tiempo_P_Cluster_Core_"

/* Lectura de una matriz de 21696 x 21696 */
#define NX 21696
#define NY 21696
#define NDIMS 2
#define NAN (float)(0.0 / 0.0)
#define MAX_BRIGHTNESS 255
#define MIN_BRIGHTNESS 0

#define KERNEL_X 3 //filas kernel
#define KERNEL_Y 3 //columnas kernel

void selectKernel(float[KERNEL_X][KERNEL_Y], float[KERNEL_X][KERNEL_Y]);
void processNAN(float *);
void loadPreFilter(float *);
void savePostFilter(float *);
void convolutionFiltering(float *, float *, float[KERNEL_X][KERNEL_Y], double *);
void findMaxMin(float *, float *, float *);
int map(int, int, int, int, int);
void checkFlag(char *, int *, int *, int *);
//void printDebug(float *);

/* ======================================================== */
/**
 * @brief Matrices de filtrado disponibles
 * 
 */
float KERNEL_EDGE_DETECTION[3][3] = {
    {0.0, -1.0, 0.0},
    {-1.0, 4.0, -1.0},
    {0.0, -1.0, 0.0}};

float KERNEL_EDGE_DETECTION_DIAGONAL[3][3] = {
    {-1.0, -1.0, -1.0},
    {-1.0, 8.0, -1.0},
    {-1.0, -1.0, -1.0}};

float KERNEL_BLUR[3][3] = {
    {1 / 9, 1 / 9, 1 / 9},
    {1 / 9, 1 / 9, 1 / 9},
    {1 / 9, 1 / 9, 1 / 9}};

float KERNEL_FOCUS[3][3] = {
    {0.0, -1.0, 0.0},
    {-1.0, 5.0, -1.0},
    {0.0, -1.0, 0.0}};

float KERNEL_IDENTITY[3][3] = {
    {0.0, 0.0, 0.0},
    {0.0, 1.0, 0.0},
    {0.0, 0.0, 0.0}};

float KERNEL_EDGE_ENHANCEMENT[3][3] = {
    {0.0, 0.0, 0.0},
    {-1.0, 1.0, 0.0},
    {0.0, 0.0, 0.0}};
/* ======================================================== */

/**
 * @brief Determina si el argumento pasado en la ejecucion del programa
 *        se corresponde con un flag valido. En ese caso lo activa.
 * 
 * @param flag valor pasado como argumento en la ejecucion del programa
 * @param i_flag flag de generacion de imagen .pgm
 * @param p_flag flag de procesamiento de NaN
 * @param n_flag flag de generacion de archivo .nc
 */
void checkFlag(char *flag, int *i_flag, int *p_flag, int *n_flag)
{
    if (!strcmp(flag, "p"))
    {
        *p_flag = 1;
        printf(" -       -p Procesar NaN -\n");
        return;
    }
    else if (!strcmp(flag, "i"))
    {
        *i_flag = 1;
        printf(" -       -i Generacion de imagen .pgm-\n");
        return;
    }
    else if (!strcmp(flag, "n"))
    {
        *n_flag = 1;
        printf(" -       -n Generacion de archivo .nc -\n");
        return;
    }
    else
    {
        printf(" - Flag -%c desconocido. Se ignora -\n", *flag);
        return;
    }
}

/**
 * @brief Procesa los NaN. Reemplaza los elementos -1 de data_in
 *        por NaN.
 * 
 * @param data_in array que contiene los datos de entrada
 */
void processNAN(float *data_in)
{
    for (int i = 0; i < NX * NY; i = i + 1)
        if (data_in[i] == (-1))
            data_in[i] = NAN;
}

/**
 * @brief Realiza la convolucion de los datos almacenados en data_in por el kernel seleccionado.
 *        Los datos se almacenan en el array data_out conforme se va realizando la convolucion.
 * 
 * @param data_out array con los datos de salida
 * @param data_in array con los datos de entrada
 * @param kernel matriz de convolucion
 */
void convolutionFiltering(float *data_out, float *data_in, float kernel[KERNEL_X][KERNEL_Y], double *tiempo)
{
    clock_t tiempo_t = clock();
    for (int fil_img = 1; fil_img < NX - 1; fil_img++) // filas
    {
        for (int col_img = 1; col_img < NY - 1; col_img++) // columnas
        {
            data_out[(fil_img * NX) + col_img] = (data_in[((fil_img - 1) * NX) + (col_img - 1)] * kernel[0][0] + data_in[((fil_img - 1) * NX) + (col_img)] * kernel[0][1] + data_in[((fil_img - 1) * NX) + (col_img + 1)] * kernel[0][2] +
                                                  data_in[(fil_img * NX) + (col_img - 1)] * kernel[1][0] + data_in[(fil_img * NX) + col_img] * kernel[1][1] + data_in[(fil_img * NX) + (col_img + 1)] * kernel[1][2] +
                                                  data_in[((fil_img + 1) * NX) + (col_img - 1)] * kernel[2][0] + data_in[((fil_img + 1) * NX) + (col_img)] * kernel[2][1] + data_in[((fil_img + 1) * NX) + (col_img + 1)] * kernel[2][2]);
        }
    }
    tiempo_t = clock() - tiempo_t;
    *tiempo = ((double)tiempo_t) / CLOCKS_PER_SEC;
}

/**
 * @brief Carga en la matriz de convolucion el filtro kernel seleccionado.
 * 
 * @param kernel kernel para realizar la convolucion
 * @param kernelType filtro seleccionado
 */
void selectKernel(float kernel[KERNEL_X][KERNEL_Y], float kernelType[KERNEL_X][KERNEL_Y])
{
    for (int k_row = 0; k_row < KERNEL_X; k_row++)
    {
        for (int k_col = 0; k_col < KERNEL_Y; k_col++)
        {
            kernel[k_row][k_col] = kernelType[k_row][k_col];
        }
    }
}

/**
 * @brief Mapea el valor actual_val procedente del rango [in_min:in_max]
 *        al rango de valores [out_min:out_max].
 * 
 * @param actual_val valor a mapear
 * @param in_min valor minimo del rango de entrada. Configurado a 0
 * @param in_max valor maximo del rango de entrada. obtenido mediante findMaxMin() 
 * @param out_min valor minimo del rango de salida. Configurado a MIN_BRIGHTNESS, 0
 * @param out_max valor maximo del rango de salida. Configurado a MAX_BRIGHTNESS, 255
 * @return int nuevo valor mapeado
 */
int map(int actual_val, int in_min, int in_max, int out_min, int out_max)
{
    return (actual_val - (int)in_min) * (out_max - out_min) / ((int)in_max - (int)in_min) + out_min;
}

/**
 * @brief Obtiene el valor minimo y maximo contenido en data_out.
 * 
 * @param data_out array a analizar
 * @param max valor maximo contenido en data_out
 * @param min valor minimo contenido en data_out
 */
void findMaxMin(float *data_out, float *max, float *min)
{
    for (int i = 0; i < NX; i++)
    {
        for (int j = 0; j < NY; j++)
        {
            if (data_out[(i * NX) + j] != NAN)
            {
                if (data_out[(i * NX) + j] > *max)
                    *max = data_out[(i * NX) + j];
                if (data_out[(i * NX) + j] < *min)
                    *min = data_out[(i * NX) + j];
            }
            else
                data_out[(i * NX) + j] = MAX_BRIGHTNESS;
        }
    }
}

/**
 * @brief Genera una imagen con extension .pgm tomando como datos
 *        los elementos de data_out
 * 
 * @param data_out 
 */
void saveAsImage(float *data_out)
{
    FILE *pgmimg;
    int temp = 0;
    float max = 0.0;
    float min = 0.0;

    pgmimg = fopen(IMG_NAME_OUT, "wb"); //pgm

    // Writing Magic Number to the File  (Identificador del tipo de archivo)
    fprintf(pgmimg, "P2\n"); //P2-->pgm

    // Writing Width and Height
    fprintf(pgmimg, "%d %d\n", NX, NY);

    /* Writing the maximum gray value . For a grayscale images, the pixel value is a single number that represents the brightness of
     the pixel. The most common pixel format is the byte image, where this number is stored as an 8-bit integer giving a range of 
     possible values from 0 to 255. Typically zero is taken to be black, and 255 is taken to be white.
   */
    fprintf(pgmimg, "255\n");

    /* Obtenemos el valor maximo y minimo almacenado en data_out */
    findMaxMin(data_out, &max, &min);

    for (int i = 0; i < NX; i++)
    {
        for (int j = 0; j < NY; j++)
        {
            if ((int)data_out[(i * NX) + j] < MIN_BRIGHTNESS)
            {
                temp = 0;
            }
            else if (MAX_BRIGHTNESS < (int)(data_out[(i * NX) + j]))
            {
                temp = map((int)data_out[(i * NX) + j], MIN_BRIGHTNESS, (int)max, MIN_BRIGHTNESS, MAX_BRIGHTNESS);
            }
            else
            {
                temp = (int)data_out[(i * NX) + j];
            }
            // Writing the gray values in the 2D array to the file
            fprintf(pgmimg, "%d ", temp);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
    printf(" - Escritura EXITOSA - Archivo generado BFilter_out.pgm -\n");
}

/**
 * @brief Almacena en un array los elementos contenidos en el archivo
 *        de entrada .nc.
 * 
 * @param data_in array generado a partir del archivo .nc
 */
void loadPreFilter(float *data_in)
{
    int ncid, varid, retval;

    printf(" - Abriendo archivo .nc -\n");
    if ((retval = nc_open(FILE_NAME_IN, NC_NOWRITE, &ncid)))
        ERR(retval);

    /* Obtenemos elvarID de la variable CMI. */
    if ((retval = nc_inq_varid(ncid, "CMI", &varid)))
        ERR(retval);

    printf(" - Leyendo datos de archivo -\n");
    /* Leemos la matriz. */
    if ((retval = nc_get_var_float(ncid, varid, data_in)))
        ERR(retval);

    /* Se cierra el archivo y liberan los recursos*/
    if ((retval = nc_close(ncid)))
        ERR(retval);
}

/**
 * @brief Genera un archivo de salida .nc a partir de los datos
 *        almacenados en el array data_out.
 * 
 * @param data_out datos a ser almacenados
 */
void savePostFilter(float *data_out)
{
    int ncid, x_dimid, y_dimid, varid, retval;
    int dimids[NDIMS];
    /* Create the file. The NC_NETCDF4 parameter tells netCDF to create
    * a file in netCDF-4/HDF5 standard. */

    printf(" - Creando archivo de salida .nc -\n");
    if ((retval = nc_create(FILE_NAME_OUT, NC_CLASSIC_MODEL, &ncid)))
        ERR(retval);

    /* Define the dimensions. */
    if ((retval = nc_def_dim(ncid, "x", NX, &x_dimid)))
        ERR(retval);
    if ((retval = nc_def_dim(ncid, "y", NY, &y_dimid)))
        ERR(retval);

    /* The dimids array is used to pass the IDs of the dimensions of
    * the variable. */
    dimids[0] = x_dimid;
    dimids[1] = y_dimid;

    /* Define the variable. The type of the variable in this case is
    * NC_FLOAT. */
    if ((retval = nc_def_var(ncid, "CMI", NC_FLOAT, NDIMS, dimids, &varid)))
        ERR(retval);

    /* End define mode. This tells netCDF kernele are done defining
    * metadata. */
    if ((retval = nc_enddef(ncid)))
        ERR(retval);

    printf(" - Almacenando datos en archivo de salida -\n");
    if ((retval = nc_put_var_float(ncid, varid, data_out)))
        ERR(retval);

    /* Close the file. */
    if ((retval = nc_close(ncid)))
        ERR(retval);
    printf(" - Escritura EXITOSA - Archivo generado BFilter_out.nc -\n");
}
