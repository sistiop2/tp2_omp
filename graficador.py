import numpy as np
import netCDF4
from netCDF4 import Dataset
import xarray
from scipy import signal
from scipy.ndimage.filters import convolve
from scipy.misc import imread, imshow
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.colors import LinearSegmentedColormap

'''
Parametros
'''
w = np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]]) #KERNEL DE CONVOLUCION

#dataDIR_original = "./recursos/OR_ABI-L2-CMIPF-M6C02_G16_s20191011800206_e20191011809514_c20191011809591.nc"
dataDIR_filter = "./recursos/BFilter_out.nc"

'''
Abrir el dataset como una matriz XARRAY y guardar la matriz CMI
'''
#DS_o = Dataset(dataDIR_original)
DS_f = Dataset(dataDIR_filter)
#fig_o = DS_o.variables['CMI']
fig_f = DS_f.variables['CMI']

print 'Generando Imagen...' 
#pixel_o = fig_o[0:21696,0:21696]
pixel_f = fig_f[0:21696,0:21696]

#plt.imshow(pixel_o[0:21696,0:21696],cmap=plt.cm.get_cmap('gray',3))
#plt.savefig('original.png',format="png",dpi=1800)
#plt.colorbar()

plt.imshow(pixel_f[0:21696,0:21696],cmap=plt.cm.get_cmap('gray',2))
plt.colorbar()
plt.savefig('filtrada.png',format="png",dpi=3600)


