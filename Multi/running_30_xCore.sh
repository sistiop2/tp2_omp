#!/bin/bash
# Se recomienda estar atento durante la ejecucion del script.
# El sistema suele activar tareas programadas ocupando los procesadores
# y desvirtuando las mediciones.

# Tener presente que los parametros 'nucleos' sean multiplos de la cantidad
# real de cores del equipo donde se ejecutara el script.
#	Ejemplo: 12 cores - nucleos 1 3 6 12 24 48 96 192 ....

for nucleos in 1 2 4 8 16 32 64 128
do
#mkdir $(HOME)/$nucleos
echo Ejecutando con: $nucleos nucleos
    for estadistica in {1..30}
    do
        ./BFilter_OMP $nucleos -p
    done
done

echo FINALIZADO...
